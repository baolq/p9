package p9.binary.cfg.controller;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.openide.util.NbBundle.Messages;

import p9.binary.cfg.display.renderer.BasicBlockRenderer;
import prefuse.visual.VisualItem;
import rreil.disassembler.Instruction;
import p9.binary.cfg.controller.Bundle;

@Messages({
  "CTL_CopyToClipboardAction=Instruction string",
  "CTL_CopyAllToClipboardAction=Basic Block string",
  "HINT_CopyToClipboardAction=Copies this instruction as string to the system clipboard.",
  "HINT_CopyAllToClipboardAction=Copies the contents of this basic block as string to the system clipboard."
})
public class ClipboardActions {
  
  private static void sendToClipboard (String content) {
    StringSelection stringSelection = new StringSelection(content);
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(stringSelection, null);
  }

  public static Action getCopyInstructionToClipboardAction (final VisualItem item, final Instruction insn) {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        String insnContent = ((BasicBlockRenderer) item.getRenderer()).asString(item, insn);
        sendToClipboard(insnContent);
      }
    };
//    action.putValue(Action.SMALL_ICON, icon);
    action.putValue(Action.NAME, Bundle.CTL_CopyToClipboardAction());
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_CopyToClipboardAction());
    return action;
  }

  public static Action getCopyBlockToClipboardAction (final VisualItem item) {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        String blockContent = ((BasicBlockRenderer) item.getRenderer()).asString(item);
        sendToClipboard(blockContent);
      }
    };
//    action.putValue(Action.SMALL_ICON, icon);
    action.putValue(Action.NAME, Bundle.CTL_CopyAllToClipboardAction());
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_CopyAllToClipboardAction());
    return action;
  }

}
