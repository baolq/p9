package p9.binary.analysis.slicing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javalx.data.products.P2;
import javalx.fn.Fn;
import javalx.mutablecollections.CollectionHelpers;
import p9.binary.cfg.InstructionsHelper;
import p9.binary.cfg.graph.AnalysisResult;
import p9.binary.cfg.graph.BasicBlock;
import p9.binary.cfg.graph.Cfg;
import p9.binary.cfg.graph.NativeCfg;
import p9.binary.cfg.graph.RReilCfg;
import prefuse.data.Node;
import rreil.disassembler.Instruction;
import rreil.lang.RReil;
import rreil.lang.RReilAddr;
import rreil.lang.Rhs.Rvar;
import rreil.lang.util.RvarExtractor;

/**
 * Performs a backwards slicing for a given variable.
 * That is, it returns the addresses of instructions that influence the value of the given variable.
 * TODO: add semantic slicing (comparing the value change instead of looking at variables in instructions only)
 *
 * @author Bogdan Mihaila
 */
public class Slicer {
  private final Cfg cfg;
  private final AnalysisResult analysis;
  private Map<BasicBlock, Node> nodesMapping;

  public Slicer (Cfg cfg, AnalysisResult analysis) {
    this.cfg = cfg;
    this.analysis = analysis;
    buildReverseMapping();
  }

  private void buildReverseMapping () {
    nodesMapping = new HashMap<>();
    Iterator<?> nodes = cfg.nodes();
    while (nodes.hasNext()) {
      Node node = (Node) nodes.next();
      BasicBlock basicBlock = (BasicBlock) node.get(Cfg.$Block);
      nodesMapping.put(basicBlock, node);
    }
  }

  private static RReil getLastInsn (Collection<RReil> instructions) {
    RReil lastInsn = instructions.iterator().next();
    for (RReil insn : instructions) {
      if (insn.getRReilAddress().compareTo(lastInsn.getRReilAddress()) > 0)
        lastInsn = insn;
    }
    return lastInsn;
  }

  private static Slice toNativeAddresses (Slice slice) {
    Slice result = new Slice();
    for (RReilAddr addr : slice) {
      result.add(addr.withOffset(0));
    }
    return result;
  }

  /**
   * Compute the slice of a CFG given a variable and native code.
   */
  public static Slice slice (Rvar variable, Instruction insn, NativeCfg cfg, AnalysisResult analysis) {
    RReilCfg rreilCfg = cfg.getRReilCfg();
    RReil rreilInsn = getLastInsn(cfg.getRReilInsns(insn));
    return toNativeAddresses(slice(variable, rreilInsn, rreilCfg, analysis));
  }

  /**
   * Compute the slice of a CFG given a variable and RREIL code.
   */
  public static Slice slice (Rvar variable, RReil insn, RReilCfg cfg, AnalysisResult analysis) {
    Slicer slicer = new Slicer(cfg, analysis);
    P2<BasicBlock, Integer> block = cfg.getBasicBlockContaining(insn.getRReilAddress());
    if (block == null)
      return null;
    return slicer.slice(variable, insn, block._1());
  }

  private Slice slice (Rvar variable, RReil insn, BasicBlock block) {
    // initialize data structures
    Set<RReilAddr> sliceAddresses = new HashSet<>();
    sliceAddresses.add(insn.getRReilAddress());
    Set<Rvar> sliceVariables = new HashSet<>();
    sliceVariables.add(variable);
    if (influencesSyntactically(sliceVariables, insn))
      sliceVariables.addAll(RvarExtractor.getRhs(insn));
    // slice for the first basic block which contains the instruction to start the slicing at
    Iterator<Instruction> iterator = adjustIterator(block.reversedOrderView().iterator(), insn);
    Iterable<Instruction> iterable = CollectionHelpers.toIterable(iterator);
    sliceBasicBlock (iterable, sliceVariables, sliceAddresses);
    // add the predecessors to the worklist and continue the slicing using that list
    Set<BasicBlock> visitedBlocks = new HashSet<>();
    visitedBlocks.add(block);
    List<BasicBlock> worklist = new LinkedList<>();
    for (BasicBlock prevBlock : predecessors(block)) {
      worklist.add(prevBlock);
    }
    while (!worklist.isEmpty()) {
      BasicBlock currBlock = worklist.remove(0);
      visitedBlocks.add(currBlock);
      boolean updated = sliceBasicBlock(currBlock.reversedOrderView(), sliceVariables, sliceAddresses);
      for (BasicBlock prevBlock : predecessors(currBlock)) {
        // continue slicing if the set of variables has grown or we have not yet visited the block
        if (updated || !visitedBlocks.contains(prevBlock))
          worklist.add(prevBlock);
      }
    }
    return new Slice(sliceAddresses);
  }

  private static boolean sliceBasicBlock (Iterable<Instruction> instructions, Set<Rvar> sliceVariables,
      Set<RReilAddr> sliceAddresses) {
    boolean updated = false;
    for (Instruction insn : instructions) {
      RReil instruction = InstructionsHelper.toRReil(insn);
      if (influencesSyntactically(sliceVariables, instruction)) {
        // TODO: see how to handle memory variables referenced by pointers
        // we need to add that variables instead of the vars containing the pointers
        boolean addedVars = sliceVariables.addAll(RvarExtractor.getRhs(instruction));
        boolean addedAddress = sliceAddresses.add(instruction.getRReilAddress());
        updated = updated || addedVars || addedAddress; // if an update happened track that
      }
    }
    return updated;
  }

  /**
   * If the instruction uses any of the variables as lhs then it influences the value of the variable.
   */
  private static boolean influencesSyntactically (Set<Rvar> variables, RReil insn) {
    List<Rvar> lhsSet = new ArrayList<>(RvarExtractor.getLhs(insn));
    lhsSet.retainAll(variables);
    return !lhsSet.isEmpty();
  }

  /**
   * Moves the iterator over the instructions in the basic block
   * to point to the previous instruction before insn.
   */
  private static Iterator<Instruction> adjustIterator (Iterator<Instruction> iterator, RReil insn) {
    while (iterator.hasNext()) {
      RReil currentInsn = InstructionsHelper.toRReil(iterator.next());
      if (currentInsn.equals(insn))
        return iterator;
    }
    throw new IllegalArgumentException("The instruction: " + insn + " was not found in the basic block");
  }

  @SuppressWarnings("unchecked") private Iterable<BasicBlock> predecessors (BasicBlock block) {
    Node node = nodesMapping.get(block);
    return CollectionHelpers.toIterable(CollectionHelpers.map(cfg.inNeighbors(node), new Fn<Node, BasicBlock>() {

      @Override public BasicBlock apply (Node node) {
        return Cfg.getBasicBlock(node);
      }

    }));
  }

}
