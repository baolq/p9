
package p9.binary.cfg.display.items;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.WeakHashMap;

/**
 * Standard layout that orders the elements from the left to the right.
 * 
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class HorizontalLayout implements LayoutManager {
  
  protected ItemSize size;

  protected List<DrawableItem> elementOrder;
  protected Map<DrawableItem, Point2D> posMap;

  public HorizontalLayout() {
    size = new ItemSize(0,0);
    elementOrder = new LinkedList<>();
    posMap = new WeakHashMap<>();
  }
  
  @Override
  public void addItem(DrawableItem item) {
    elementOrder.add(item);
    updateLayout();
  }
  
  @Override
  public void addAllItems(Iterable<DrawableItem> items) {
    for(DrawableItem di : items) {
      elementOrder.add(di);
    }
    updateLayout();
  }
  
  @Override
  public void insertItem(DrawableItem item, int index) {
    elementOrder.add(index, item);
    updateLayout();
  }
  
  @Override
  public void removeItem(int index) {
    elementOrder.remove(index);
    updateLayout();
  }
  
  public void removeItem(DrawableItem item) {
    elementOrder.remove(item);
    updateLayout();
  }
  
  @Override
  public void removeAllItems() {
    elementOrder = new LinkedList<>();
    updateLayout();
  }
  
  public void replaceItem(DrawableItem item, int index) {
    elementOrder.set(index, item);
    updateLayout();
  }
  
  public boolean isEmpty() {
    return elementOrder.isEmpty();
  }
  
  @Override
  public Point2D getPosition(DrawableItem item) {
    Point2D pos = posMap.get(item);
    if(pos == null)
      throw new NoSuchElementException();
    
    return pos;
  }

  @Override
  public synchronized void updateLayout() {
    double xPos = 0;
    double yMax = 0;

    for (DrawableItem i : elementOrder) {
      ItemSize d;
      try {
        d = i.getItemSize();
      } catch (NullPointerException e) {
        continue;
      }
      double nextXPos = xPos + d.getWidth();
      posMap.put(i, new Point2D.Double(xPos, 0));
      xPos = nextXPos;
      yMax = (d.getHeight() > yMax) ? d.getHeight() : yMax;
    }

    size = new ItemSize(xPos, yMax);
  }

  @Override
  public ItemSize getLayoutSize() {
    return size;
  }

  @Override
  public void setLayoutSize(ItemSize size) {
    this.size = size;
  }

  @Override
  public Iterator<DrawableItem> iterator() {
    return elementOrder.iterator();
  }

  // return the item at coordinates #position in the layout
  @Override
  public DrawableItem getItemAt(Point2D position) {
    for (DrawableItem i : elementOrder) {
      if (i == null) {
        continue;
      }
      Rectangle2D itemBox = new Rectangle2D.Double();
      itemBox.setFrame(posMap.get(i), i.getItemSize());

      if (itemBox.contains(position)) {
        return i;
      }
    }

    return null;
  }

  @Override
  public Point2D getRelativePos(Point2D position, DrawableItem item) {
    Point2D itemPos = posMap.get(item);
    if (itemPos == null) {
      throw new NoSuchElementException();
    }
    return new Point2D.Double(position.getX() - itemPos.getX(),
            position.getY() - itemPos.getY());
  }

  @Override
  public int getIndex(DrawableItem item) {
    return elementOrder.indexOf(item);
  }
  
  @Override
  public int numberOfElements() {
    return elementOrder.size();
  }
}
