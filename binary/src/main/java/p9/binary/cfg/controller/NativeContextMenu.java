package p9.binary.cfg.controller;

import java.awt.event.ActionEvent;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;

import org.openide.util.ImageUtilities;

import p9.binary.cfg.InstructionsHelper;
import p9.binary.cfg.controller.events.CfgViewEventBus;
import p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressEvent;
import p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressInNativeCfgEvent;
import p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressInRReilCfgEvent;
import p9.binary.cfg.display.CfgComponent;
import p9.binary.cfg.graph.Cfg;
import prefuse.visual.NodeItem;
import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;
import rreil.lang.Rhs.Rvar;
import p9.binary.cfg.controller.Bundle;
import bindead.analyses.BinaryCodeCache;
import bindead.analyses.algorithms.data.ProgramCtx;

public class NativeContextMenu extends BasicBlockContextMenu {
  private static final ImageIcon showInOtherIcon = ImageUtilities.loadImageIcon(iconPath, false);

  public NativeContextMenu (Cfg cfg, String functionName, String blockSchemaID) {
    super(cfg, functionName, blockSchemaID);
  }

  @Override protected JPopupMenu handleCfgNode (NodeItem item) {
    Instruction selectedInstruction = getSelectedInstruction(item);
    JPopupMenu menu = new JPopupMenu();

    try {
      RReilAddr selectedAddress = selectedInstruction.address();
      Set<Rvar> variables = InstructionsHelper.getOccurringVariables(selectedInstruction, analysis.getAnalysis().getPlatform());
      addShowDomainAction(selectedAddress, variables, menu);
      addCopyToClipboardActions(item, selectedInstruction, menu);
      addShowInOtherAction(selectedAddress, menu);
      addFollowCallsAndReturnsActions(selectedAddress, menu);
    } catch(NullPointerException e) { /* item is not an instruction */ }

    return menu;
  }

  private void addShowInOtherAction (final RReilAddr address, JPopupMenu menu) {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        CfgFocusAddressEvent event = new CfgFocusAddressInRReilCfgEvent(analysis, new ProgramCtx(callString, address));
        CfgViewEventBus.getInstance().publish(event);
      }
    };
    String cfgType = "RREIL";
    action.putValue(Action.SMALL_ICON, showInOtherIcon);
    action.putValue(Action.NAME, Bundle.CTL_ShowInCfgAction(cfgType));
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_ShowInCfgAction(cfgType));
    menu.add(action);
  }

  private void addFollowCallsAndReturnsActions (final RReilAddr address, JPopupMenu menu) {
    if (calls.containsNativeKey(address)) {
      ProgramCtx target = calls.getNative(address);
      Action action = buildFollowCallsReturnsAction(target);
      BinaryCodeCache binaryCodeView = analysis.getAnalysis().getBinaryCode();
      String architecture = binaryCodeView != null ? binaryCodeView.getBinary().getArchitectureName() : "RREIL";
      CfgComponent.setCfgOpenActionDescription(action, architecture, "called ");
      menu.add(action);
    }
    if (returns.containsNativeKey(address)) {
      ProgramCtx target = returns.getNative(address);
      Action action = buildFollowCallsReturnsAction(target);
      BinaryCodeCache binaryCodeView = analysis.getAnalysis().getBinaryCode();
      String architecture = binaryCodeView != null ? binaryCodeView.getBinary().getArchitectureName() : "RREIL";
      CfgComponent.setCfgOpenActionDescription(action, architecture, "returned to ");
      menu.add(action);
    }
  }

  private Action buildFollowCallsReturnsAction (final ProgramCtx target) {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        CfgFocusAddressEvent event = new CfgFocusAddressInNativeCfgEvent(analysis, target);
        CfgViewEventBus.getInstance().publish(event);
      }
    };
    return action;
  }

}
