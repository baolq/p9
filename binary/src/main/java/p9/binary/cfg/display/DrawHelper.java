package p9.binary.cfg.display;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;

/**
 * Common code for drawing items.
 *
 * @author Bogdan Mihaila
 */
public class DrawHelper {
  private static final double highlightRectArcRadius = 2;

  public static void drawRectangleHighligh (Graphics2D g, Point2D position, double width, double height) {
    for (double i = highlightRectArcRadius; i > 0; i--) {
      double factor = 1 - i / highlightRectArcRadius;
      g.setColor(new Color((int) (factor * 255), (int) (factor * 255), (int) (factor * 255)));
      g.draw(new RoundRectangle2D.Double(
        position.getX() - i, position.getY() - i,
        width + 2 * i + 2, height + 2 * i,
        i, i));
    }
  }

}
