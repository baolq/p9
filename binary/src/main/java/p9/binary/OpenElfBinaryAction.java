package p9.binary;

import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.loaders.DataObject;
import org.openide.util.NbBundle.Messages;

import p9.binary.cfg.display.CfgComponent;
import binparse.elf.ElfBinary;

@ActionID(category = "File", id = "p9.binary.OpenElfBinaryAction")
@ActionRegistration(lazy = false, iconBase = CfgComponent.iconPath, displayName = "#CTL_OpenElfBinaryAction")
@ActionReferences({@ActionReference(path = "Loaders/application/x-elf-object/Actions", position = 0)})
@Messages("CTL_OpenElfBinaryAction=Analyze")
public final class OpenElfBinaryAction extends OpenBinaryAction {

  public OpenElfBinaryAction () {
    super(null);
  }

  public OpenElfBinaryAction (DataObject context) {
    super(context, ElfBinary.getFactory());
  }

}
