package prefuse.render;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import prefuse.Constants;
import prefuse.data.Schema;
import prefuse.util.ArrayLib;
import prefuse.util.GraphicsLib;
import prefuse.visual.EdgeItem;
import prefuse.visual.VisualItem;

/**
 * @author Bogdan Mihaila
 */
public class RoutedEdgeRenderer extends EdgeRenderer {
  /**
   * Default data field for storing edge routing (point array) values.
   */
  public static final String $EdgeRoutingKey = "EdgeRouting";
  /**
   * A Schema describing the edge routing specification.
   */
  public static final Schema $EdgeRoutingSchema = new Schema();
  static {
    $EdgeRoutingSchema.addColumn($EdgeRoutingKey, Point2D[].class);
  }

  private final Path2D path = new Path2D.Double();
  private boolean updateRoutePoints = false;

  public RoutedEdgeRenderer () {
    super();
  }

  public RoutedEdgeRenderer (int edgeTypeCurve) {
    super(edgeTypeCurve);
  }

  public RoutedEdgeRenderer (int edgeType, int arrowType) {
    super(edgeType, arrowType);
  }

  @Override protected Shape getRawShape (VisualItem item) {
    switch (m_edgeType) {
    case Constants.EDGE_TYPE_LINE:
      return super.getRawShape(item);
    case Constants.EDGE_TYPE_CURVE:
      return super.getRawShape(item);
    case Constants.EDGE_TYPE_POLYLINE:
      return drawPolyline(item, false);
    case Constants.EDGE_TYPE_POLYLINE_CURVE:
      return drawPolyline(item, true);
    default:
      throw new IllegalStateException("Unknown edge type");
    }
  }

  private Shape drawPolyline (VisualItem item, boolean curved) {
    Point2D[] routePoints = null;
    if (item.canGet($EdgeRoutingKey, Point2D[].class))
      routePoints = (Point2D[]) item.get($EdgeRoutingKey);
    if (routePoints == null) {
      setEdgeType(Constants.EDGE_TYPE_LINE);
      Shape shape = getRawShape(item);
      setEdgeType(Constants.EDGE_TYPE_POLYLINE_CURVE);
      return shape;
    }
    EdgeItem edge = (EdgeItem) item;
    if (updateRoutePoints) {
      routePoints = removeRoutePointsInsideNode(edge.getSourceItem().getBounds(), routePoints);
      routePoints = removeRoutePointsInsideNode(edge.getTargetItem().getBounds(), routePoints);
      if (routePoints.length == 0)
        return null;
      routePoints = smoothOutRoutePoints(routePoints);
    }
    routePoints = fixupEdgeRoutePointsToNodes(edge, routePoints);
    createArrow(edge, routePoints);
    if (updateRoutePoints)
      item.set($EdgeRoutingKey, routePoints);
    return createPolyline(routePoints, curved);
  }

  private static Point2D[] fixupEdgeRoutePointsToNodes (EdgeItem edge, Point2D[] routePoints) {
    Rectangle2D sourceNode = edge.getSourceItem().getBounds();
    routePoints = addNodeCollisionPoint(sourceNode, routePoints);
    ArrayLib.reverseArray(routePoints);
    Rectangle2D targetNode = edge.getTargetItem().getBounds();
    routePoints = addNodeCollisionPoint(targetNode, routePoints);
    ArrayLib.reverseArray(routePoints);
    return routePoints;
  }

  /**
   * Removes route points that are too close together to avoid fuzzy lines.
   */
  private static Point2D[] smoothOutRoutePoints (Point2D[] routePoints) {
    if (routePoints.length <= 2)
      return routePoints;
    int minDistance = 10;
    Point2D[] tmpRoutePoints = new Point2D[routePoints.length];
    int mainIndex = 0;
    int cursor = 1;
    int tmpIndex = 0;
    tmpRoutePoints[0] = routePoints[0];
    while (mainIndex + cursor < routePoints.length) {
      Point2D point1 = routePoints[mainIndex];
      Point2D point2 = routePoints[mainIndex + cursor];
      double distance = point1.distance(point2);
      if (Math.abs(distance) > minDistance) {
        tmpRoutePoints[tmpIndex] = point2;
        tmpIndex = tmpIndex + 1;
        mainIndex = mainIndex + cursor;
        cursor = 1;
      } else {
        cursor++;
      }
    }
    if (tmpIndex < 2)
      return routePoints;
    Point2D[] newRoutePoints = new Point2D[tmpIndex];
    System.arraycopy(tmpRoutePoints, 0, newRoutePoints, 0, tmpIndex);
    return newRoutePoints;
  }

  private static Point2D[] removeRoutePointsInsideNode (Rectangle2D node, Point2D[] routePoints) {
    Point2D[] tmpRoutePoints = new Point2D[routePoints.length];
    int tmpIndex = 0;
    for (int i = 0; i < routePoints.length; i++) {
      Point2D point = routePoints[i];
      if (!node.contains(point)) {
        tmpRoutePoints[tmpIndex] = point;
        tmpIndex++;
      }
    }
    Point2D[] newRoutePoints = new Point2D[tmpIndex];
    System.arraycopy(tmpRoutePoints, 0, newRoutePoints, 0, tmpIndex);
    return newRoutePoints;
  }

  private static Point2D[] addNodeCollisionPoint (Rectangle2D node, Point2D[] routePoints) {
    int outsidePointIndex = getIntersectingLineSegment(routePoints, node);
    if (outsidePointIndex == -1)
      return routePoints;
    Point2D firstPointOutsideNode = routePoints[outsidePointIndex];
    Point2D firstPointInsideNode;
    if (outsidePointIndex - 1 >= 0)
      firstPointInsideNode = routePoints[outsidePointIndex - 1];
    else
      firstPointInsideNode = new Point2D.Double(node.getCenterX(), node.getCenterY());

    Point2D[] intersectionPoints = new Point2D[2];
    // take the intersection of the line with the node
    int resultCode =
      GraphicsLib.intersectLineRectangle(firstPointOutsideNode, firstPointInsideNode, node, intersectionPoints);
    assert resultCode > 0 : "Something went really wrong.";
    Point2D nodeIntersectionPoint = intersectionPoints[0];
    // this should avoid singularities
    double distance = firstPointOutsideNode.distance(nodeIntersectionPoint);
    int epsilon = 3;
    int newRouteLength = routePoints.length - outsidePointIndex + 1; // one more for the intersection point
    if (Math.abs(distance) <= epsilon) {
      // absolute value is smaller than epsilon so replace the first outside point with the intersection
      newRouteLength = newRouteLength - 1;
      outsidePointIndex = outsidePointIndex + 1;
    }
    Point2D[] newRoutePoints = new Point2D[newRouteLength];
    newRoutePoints[0] = nodeIntersectionPoint;
    System.arraycopy(routePoints, outsidePointIndex, newRoutePoints, 1, newRouteLength - 1);
    return newRoutePoints;
  }

  /**
   * Searches which segment of a polyline intersects a given rectangle. Returns the first one encountered while starting
   * the search at the beginning of the list (polyline).
   *
   * @param polylinePoints A polyline to be intersected with a rectangle
   * @param rectangle The rectangle to be intersected with
   * @return The index into the points array of the first point that is not inside the rectangle
   */
  private static int getIntersectingLineSegment (Point2D[] polylinePoints, Rectangle2D rectangle) {
    int index = 0;
    do {
      Point2D point = polylinePoints[index];
      if (!rectangle.contains(point))
        return index;
      index = index + 1;
    } while (index < polylinePoints.length);
    return -1;
  }

  /**
   * Create the arrow shape.
   *
   * @param edge The edge to create the arrow for
   * @param routePoints Routing points for the edge. First and last points in the array should be on the margins of the
   *          edge's end nodes.
   */
  private void createArrow (EdgeItem edge, Point2D[] routePoints) {
    m_curWidth = (float) (m_width * getLineWidth(edge));
    if (edge.isDirected() && m_edgeArrow != Constants.EDGE_ARROW_NONE && routePoints.length >= 2) {
      boolean forward = (m_edgeArrow == Constants.EDGE_ARROW_FORWARD);
      Point2D pointOnNode;
      Point2D pointOutsideNode;
      if (forward) {
        pointOnNode = routePoints[routePoints.length - 1];
        pointOutsideNode = routePoints[routePoints.length - 2];
      } else {
        pointOnNode = routePoints[0];
        pointOutsideNode = routePoints[1];
      }
      AffineTransform at = getArrowTrans(pointOutsideNode, pointOnNode, 1);
      m_curArrow = at.createTransformedShape(m_arrowHead);
      // update the endpoints for the edge shape to remove drawing more than the arrow length
      Point2D pointOnArrow = new Point2D.Double(0, -m_arrowHeight);
      at.transform(pointOnArrow, pointOnArrow);
      if (forward) {
        routePoints[routePoints.length - 1] = pointOnArrow;
      } else {
        routePoints[0] = pointOnArrow;
      }
    } else {
      m_curArrow = null;
    }
  }

  /**
   * Builds a path out of an array of points. The edges in the path can be rounded.
   *
   * Taken from JGraphX: com.mxgraph.canvas.mxGraphics2DCanvas.paintPolyline(mxPoint[], boolean)
   */
  private Shape createPolyline (Point2D[] routePoints, boolean roundedEdges) {
    if (routePoints == null || routePoints.length <= 1)
      return null;

    Point2D pt = routePoints[0];
    Point2D pe = routePoints[routePoints.length - 1];
    double arcSize = 10;
    path.reset();
    path.moveTo(pt.getX(), pt.getY());

    // Draws the line segments
    for (int i = 1; i < routePoints.length - 1; i++) {
      Point2D tmp = routePoints[i];
      double dx = pt.getX() - tmp.getX();
      double dy = pt.getY() - tmp.getY();

      if ((roundedEdges && i < routePoints.length - 1) && (dx != 0 || dy != 0)) {
        // Draws a line from the last point to the current
        // point with a spacing of size off the current point
        // into direction of the last point
        double dist = Math.sqrt(dx * dx + dy * dy);
        double nx1 = dx * Math.min(arcSize, dist / 2) / dist;
        double ny1 = dy * Math.min(arcSize, dist / 2) / dist;

        double x1 = tmp.getX() + nx1;
        double y1 = tmp.getY() + ny1;
        path.lineTo(x1, y1);

        // Draws a curve from the last point to the current
        // point with a spacing of size off the current point
        // into direction of the next point
        Point2D next = routePoints[i + 1];
        dx = next.getX() - tmp.getX();
        dy = next.getY() - tmp.getY();

        dist = Math.max(1, Math.sqrt(dx * dx + dy * dy));
        double nx2 = dx * Math.min(arcSize, dist / 2) / dist;
        double ny2 = dy * Math.min(arcSize, dist / 2) / dist;

        double x2 = tmp.getX() + nx2;
        double y2 = tmp.getY() + ny2;

        path.quadTo(tmp.getX(), tmp.getY(), x2, y2);
        tmp = new Point2D.Double(x2, y2);
      } else {
        path.lineTo(tmp.getX(), tmp.getY());
      }
      pt = tmp;
    }
    path.lineTo(pe.getX(), pe.getY());
    return path;
  }

  /**
   * If this property is set the route points of an edge are removed or new points inserted when the node is moved.
   *
   * @return <code>true</code> if edge route points are updated on the fly when moving nodes
   */
  public boolean getUpdateRoutePointsOnNodeMove () {
    return updateRoutePoints;
  }

  /**
   * If a node is moved then the route points of its edges can be updated on the fly. This has a small performance
   * penalty and it might not be desired that route points are removed/inserted. Thus the default is <code>false</code>.
   *
   * @param discard <code>true</code> if the route points should be modified on the fly
   */
  public void setUpdateRoutePointsOnNodeMove (boolean update) {
    updateRoutePoints = update;
  }

}
