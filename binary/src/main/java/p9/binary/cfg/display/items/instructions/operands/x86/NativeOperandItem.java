
package p9.binary.cfg.display.items.instructions.operands.x86;

import java.util.LinkedList;
import java.util.List;

import p9.binary.cfg.display.items.ContainerItem;
import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.display.items.HorizontalLayout;
import p9.binary.cfg.display.items.TextItem;
import javalx.numeric.Interval;
import rreil.disassembler.OperandInfixIterator;
import rreil.disassembler.OperandTree;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class NativeOperandItem extends ContainerItem {

  protected final OperandTree operand;
  protected final boolean numbersInHex;

  public NativeOperandItem(OperandTree operand, boolean numbersInHex) {
    super(new HorizontalLayout());
    this.operand = operand;
    this.numbersInHex = numbersInHex;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    String strSeparator = " ";

    boolean first = true;
    for(DrawableItem item: this) {
      if(first)
        first = false;
      else
        builder.append(strSeparator);

      builder.append(item.toString());
    }

    return builder.toString();
  }

  public void updateOperands() {
    removeAllItems();
    addAllItems(operandsToItems(operand, numbersInHex, false));
  }

  protected static List<DrawableItem> operandsToItems(OperandTree ot, boolean numbersInHex, boolean pointer) {
    List<DrawableItem> result = new LinkedList<>();

    // code copied from X86PrettyPrinter and customized for items
    OperandInfixIterator it = new OperandInfixIterator(ot.getRoot());
    int size = 64;

    while (it.next()) {
      OperandTree.Node node = it.current();
      switch (node.getType()) {
      case Immf:
      case Immi:
        result.add(new ImmediateItem(formatNumber((Number) node.getData(), numbersInHex, size, pointer)));
        break;
      case Immr:
        result.add(new ImmediateItem(((Interval) node.getData()).toString()));
        break;
      case Sym:
        result.add(new RegisterItem(node.getData().toString())); // Hack for GDSL
        break;
      case Size:
        size = ((Number) node.getData()).intValue();
        break;
      case Op:
        result.add(new TextItem(node.getData().toString().toLowerCase().replaceAll("dword ptr", "DWORD PTR"))); // Hack for GDSL
        break;
      case Mem:
        // mem case is handled outside of this iterator
        break;
      }
    }

    return result;
  }

  private static String formatNumber (Number number, boolean hex, int size, boolean showSign) {
    String sign = "";
    long longValue = number.longValue();
    if (showSign && longValue < 0) {
      sign = "-";
      longValue = -longValue;
    }
    String numString;
    if (hex) {
      String hexString = Long.toHexString(longValue);
      // cut off negative hex strings (two complement representation) to the right size
      if (hexString.length() > size / 4)
        hexString = hexString.substring(hexString.length() - size / 4);
      numString = sign + "0x" + hexString;
    } else {
      numString = number.toString();
    }
    return numString;
  }
}
