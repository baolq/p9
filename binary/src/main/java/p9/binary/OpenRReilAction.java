package p9.binary;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.util.Cancellable;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.actions.ActionInvoker;
import org.openide.util.actions.Presenter;

import p9.binary.analysis.Analyzer;
import p9.binary.cfg.controller.events.CfgViewEventBus;
import p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressEvent;
import p9.binary.cfg.display.CallTreeComponent;
import p9.binary.cfg.display.CfgComponent;
import p9.binary.cfg.graph.AnalysisResult;
import rreil.lang.RReil;
import rreil.lang.RReilAddr;
import bindead.analyses.Analysis;
import bindead.analyses.ProgressReporter;
import bindead.analyses.algorithms.data.CallString;
import bindead.exceptions.CallStringAnalysisException;
import binparse.rreil.RReilBinary;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import p9.binary.cfg.display.progressdialog.InteractiveIterationsDiagram;

@ActionID(category = "File", id = "p9.binary.OpenRReilAction")
@ActionRegistration(lazy = false, iconBase = CfgComponent.iconPath, displayName = "#CTL_OpenRReilAction")
@ActionReferences({
    @ActionReference(path = "Loaders/text/x-rreil/Actions", position = 0)})
@Messages({
    "CTL_OpenRReilAction=Analyze",
    "HINT_OpenRReilAction=Reconstruct the CFGs for this file."
})
public class OpenRReilAction extends AbstractAction implements ContextAwareAction, Presenter.Popup {
    protected static final String iconPath = CfgComponent.iconPath;
    private static final ImageIcon icon = ImageUtilities.loadImageIcon(iconPath, false);
    private final FileObject file;

    // no argument constructor necessary for context aware actions
    public OpenRReilAction() {
        this(null);
    }

    public OpenRReilAction(RReilFileDataObject dataObject) {
        file = dataObject != null ? dataObject.getPrimaryFile() : null;
        init();
    }

    private void init() {
        setEnabled(file != null);
        putValue(Action.SMALL_ICON, icon);
        putValue(Action.NAME, Bundle.CTL_OpenRReilAction());
        putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_OpenRReilAction());
    }

    @Override
    public Action createContextAwareInstance(Lookup actionContext) {
        RReilFileDataObject fileObject = actionContext.lookup(RReilFileDataObject.class);
        return new OpenRReilAction(fileObject);
    }

    @Override
    public JMenuItem getPopupPresenter() {
        return new JMenuItem(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      ActionInvoker.invokeAction(null, e, true, new Runnable() {
        @Override public void run () {
          performAnalysis();
        }
      });
    }

    private void performAnalysis() {
        final Thread currentThread = Thread.currentThread();
        final ProgressHandle progressBar = ProgressHandleFactory.createHandle("Reconstruction", new Cancellable() {
            @Override
            public boolean cancel() {
                currentThread.interrupt();
                return true;
            }
        });
        // create the bar chart
        InteractiveIterationsDiagram barChartComponent = new InteractiveIterationsDiagram();
        // create the progress bar
        JComponent progressComponent = ProgressHandleFactory.createProgressComponent(progressBar);
        // create the status label
        JLabel progressLabel = ProgressHandleFactory.createDetailLabelComponent(progressBar);
        // create the dialog window
        DialogDescriptor dd = new DialogDescriptor(
                new AnalysisProgressPanel(progressComponent, progressLabel, barChartComponent),
                ProgressHandleFactory.createMainLabelComponent(progressBar).getText());
        // display the dialog
        DialogDisplayer.getDefault().notifyLater(dd);
        progressBar.start();
        progressBar.switchToIndeterminate();
        progressBar.setDisplayName("Reconstructing CFGs ...");
        try {
          if (file == null)
            throw new IllegalArgumentException("No binary provided to analyze.");
          final Analyzer analyzer = new Analyzer(new RReilBinary(file.getPath()));
          final ProgressReporter progressMonitor = analyzer.getAnalysis().getProgressMonitoring();
          
          barChartComponent.setProgressReporter(progressMonitor);
          progressMonitor.addInstructionListener(barChartComponent);
      
          progressMonitor.addInstructionListener(new ProgressReporter.InstructionListener() {
            int step = 0;
            @Override public void evaluatingInstruction(RReil insn) {
              int numberOfWarnings = progressMonitor.getWarnings().totalNumberOfWarnings();
              progressBar.progress(String.format( "step #%d,  insn: %s,  warnings: %d",
                      step++, insn.toString(), numberOfWarnings));
            }
          });
          performReconstruction(analyzer);
        } catch (CallStringAnalysisException e) {
            Exceptions.printStackTrace(e);
            final String fileName = file.getName();
            final AnalysisResult analysis = new AnalysisResult(e.getAnalysis(), fileName);
            final CallString callString = e.getCallString();
            final RReilAddr address = e.getAddress();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Action openCallTreeAction = CallTreeComponent.getOpenAction(analysis, fileName);
                    openCallTreeAction.actionPerformed(null);
                    Action openRReilCfgAction = CfgComponent.getOpenRReilCfgAction(analysis, callString);
                    openRReilCfgAction.actionPerformed(null);
                    String cfgID = CfgViewEventBus.getRReilEventsID(analysis, callString);
                    CfgFocusAddressEvent event = new CfgFocusAddressEvent(cfgID, address, null);
                    CfgViewEventBus.getInstance().publish(event);
                }
            });
        } catch (Exception e) {
            Exceptions.printStackTrace(e);
        } finally {
            progressBar.finish();
        }
    }

    private static void performReconstruction (Analyzer analyzer) {
      Analysis<?> analysis = analyzer.runAnalysis();
      final AnalysisResult result = new AnalysisResult(analysis, analyzer.getBinary().getFileName());
      final String fileName = analyzer.getBinary().getFileName();
      SwingUtilities.invokeLater(new Runnable() {
        @Override public void run () {
          Action openCallTreeAction = CallTreeComponent.getOpenAction(result, fileName);
          openCallTreeAction.actionPerformed(null);
        }
      });
    }

}
