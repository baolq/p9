package p9.binary.cfg.display.items.instructions;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.io.StringWriter;
import java.text.AttributedString;

import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.display.items.ItemSize;
import bindead.domainnetwork.channels.WarningMessage;
import bindead.domainnetwork.channels.WarningsContainer;

import com.googlecode.jatl.Html;

/**
 * @author Raphael Dümig
 */
public class InstructionDecoratorItem extends DrawableItem {
  private final double bgarcw = 1.5;
  private final double bgarch = 1.5;

  private Shape numberBg;
  private TextLayout numberText;
  private int nwarnings;
  private WarningsContainer warnings;
  private Font font;
  private int iterationCount;

  public InstructionDecoratorItem() {
    this.numberBg = null;
    this.nwarnings = -1;
    this.warnings = null;
    this.font = null;
    this.numberText = null;

    setPadding(PADDING_HORIZONTAL, 2);
  }

  public void setWarnings(WarningsContainer warnings, int iterationCount) {
    if (warnings == null) {
      this.nwarnings = 0;
      this.warnings = null;
    } else {
      this.nwarnings = warnings.size();
      this.warnings = warnings;
    }
    this.iterationCount = iterationCount;
    updateGraphics();
  }

  private void updateGraphics () {
    // update the text layout
    if (nwarnings <= 0) {
      numberText = null;
    } else if (font != null) {
      numberText = new TextLayout(Integer.toString(nwarnings), font, defaultContext.getFontRenderContext());
    } else {
      AttributedString as = new AttributedString(Integer.toString(nwarnings));
      as.addAttribute(TextAttribute.FOREGROUND, Color.black);
      as.addAttribute(TextAttribute.WEIGHT, Font.BOLD);
      numberText = new TextLayout(as.getIterator(), defaultContext.getFontRenderContext());
    }

    // update the background
    if (numberText != null) {
      double layoutWidth = numberText.getBounds().getWidth() + 2 * bgarcw;
      double layoutHeight = numberText.getAscent() + numberText.getDescent() + 2 * bgarch;
      numberBg = new RoundRectangle2D.Double(0, 0, layoutWidth, layoutHeight, 2 * bgarcw, 2 * bgarch);
    } else {
      numberBg = null;
    }
  }

  @Override
  protected ItemSize getMinimumSize() {
    if (nwarnings > 0) {
      Rectangle2D bounds = numberBg.getBounds2D();
      return new ItemSize(bounds.getWidth(), bounds.getHeight());
    } else {
      return new ItemSize(0, 0);
    }
  }

  @Override
  public void setFont(Font f) {
    this.font = new Font(f.getName(), Font.BOLD, 8);
    updateGraphics();
  }

  @Override
  protected void draw(Graphics2D g, Point2D position) {
    if (nwarnings > 0) {
      drawWarningsDecorator(g, position);
    }
  }

  private void drawWarningsDecorator(Graphics2D g, Point2D itemPos) {
    g.translate(itemPos.getX(), itemPos.getY());
    Color oldColor = g.getColor();
    Font oldFont = g.getFont();

    g.setColor(Color.white);
    if (font != null) {
      g.setFont(font);
    }

    g.fill(numberBg);
    g.setColor(Color.black);
    double bgWidth = numberBg.getBounds2D().getWidth();
    double layoutWidth = numberText.getBounds().getWidth();
    numberText.draw(g, (float) (bgWidth - layoutWidth)/2, (float) (numberText.getAscent() + bgarch));

    g.setColor(oldColor);
    g.setFont(oldFont);
    g.translate(-itemPos.getX(), -itemPos.getY());
  }

  @Override
  public String tooltipText() {
    if (nwarnings > 0) {
      final StringWriter htmlStringWriter = new StringWriter();
      new Html(htmlStringWriter) {
        {
          indent(indentOff); // needed as Swing Tooltips HTML seem to choke on tab indented HTML strings
          html();
          body();
          // headline
          p().style("padding-bottom: 3px;");
          text(nwarnings + ((nwarnings > 1) ? " warnings" : " warning"));
          text(" in iteration " + iterationCount + ":");
          end(); // p
          ul();
          for (WarningMessage message : warnings) {
            li().text(message.detailedMessage()).end();
          }
          end(); // ul
          end(); // body
          end(); // html
          done();
        }
      };
      return htmlStringWriter.toString();
    }
    return "";
  }
}
