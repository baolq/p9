package p9.binary.cfg.display.items.highlight;

import rreil.lang.Rhs;

/**
 *
 * @author Raphael Dümig
 */
public class VariableHighlight implements DataHighlight<Rhs> {
  private final Rhs var;

  public VariableHighlight(Rhs var) {
    this.var = var;
  }
  
  @Override
  public boolean contains(Rhs itemData) {
    return itemData.equals(var);
  }
  
}
