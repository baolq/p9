package p9.binary.cfg.controller;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import p9.binary.cfg.display.items.BasicBlockItem;
import p9.binary.cfg.display.renderer.BasicBlockRenderer;
import prefuse.controls.ControlAdapter;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;

/**
 * @author Raphael Dümig
 */
public class BasicBlockHoverControl extends ControlAdapter {
  private VisualItem currentItem;
  private BasicBlockRenderer currentRenderer;
  private BasicBlockItem currentBlockItem;

  public BasicBlockHoverControl (String blockSchemaID) {
    this.currentItem = null;
    this.currentRenderer = null;
    this.currentBlockItem = null;
  }

  private void updateVisualItem (VisualItem item) {
    currentItem = item;
    if (item == null) {
      currentRenderer = null;
      currentBlockItem = null;
    } else {
      currentRenderer = (BasicBlockRenderer) currentItem.getRenderer();
      currentBlockItem = currentRenderer.getBlockItem();
    }
  }


  @Override
  public void itemEntered (VisualItem item, MouseEvent e) {
    if (!(item instanceof NodeItem))
      return;
    updateVisualItem(item);
    
    // get the mouse position relative to the origin of the item
    Point2D mousePosition = currentRenderer.getMousePosition(currentItem);
    currentBlockItem.mouseEntered(e, mousePosition);
  }

  @Override
  public void itemExited (VisualItem item, MouseEvent e) {
    if (!(item instanceof NodeItem))
      return;
    assert (item == currentItem);
    
    currentBlockItem.mouseExited(e, null);
    updateVisualItem(null);
  }

  @Override public void itemMoved (VisualItem item, MouseEvent e) {
    if (!(item instanceof NodeItem))
      return;
    assert (item == currentItem);
    
    Point2D mousePosition = currentRenderer.getMousePosition(currentItem);
    currentBlockItem.mouseMoved(e, mousePosition);
  }
  
  @Override public void itemClicked (VisualItem item, MouseEvent e) {
    if (!(item instanceof NodeItem))
      return;
    assert (item == currentItem);
    
    Point2D mousePosition = currentRenderer.getMousePosition(currentItem);
    currentBlockItem.mouseClicked(e, mousePosition);
  }
  
  public BasicBlockItem getHoveredBlockItem () {
    return currentBlockItem;
  }
}
