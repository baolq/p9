package p9.binary.cfg.display;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

public class RunAfter {

  public static void delay (int delay, final Runnable task) {
    Timer timer = new Timer(delay, new ActionListener() {
      @Override public void actionPerformed (ActionEvent e) {
        task.run();
      }
    });
    timer.setRepeats(false);
    timer.start();
  }

}
