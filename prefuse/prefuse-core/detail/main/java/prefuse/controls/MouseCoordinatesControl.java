package prefuse.controls;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import prefuse.Display;
import prefuse.data.Schema;
import prefuse.visual.VisualItem;

/**
 * A control that saves the relative mouse position to an item when the mouse moves over an item. The relative mouse position
 * is the position of the mouse seen as the distance from the item's upper left corner.
 */
public class MouseCoordinatesControl extends ControlAdapter {
  public static final String mousePositionKey = "mousePosition";
  public static final Schema mousePositionSchema = new Schema(new String[] {mousePositionKey},
    new Class[] {Point2D.class});
  private final String activity;

  public MouseCoordinatesControl () {
    this(null);
  }

  /**
   * Creates a new control that runs the given activity whenever the mouse moves over an item line number changes.
   *
   * @param activity the update Activity to run
   */
  public MouseCoordinatesControl (String activity) {
    this.activity = activity;
  }

  @Override public void itemMoved (VisualItem item, MouseEvent e) {
    handleItem(item, e);
  }

  @Override public void itemEntered (VisualItem item, MouseEvent e) {
    handleItem(item, e);
  }

  private static Point2D outsidePoint = new Point2D.Float(-1, -1);

  @Override public void itemExited (VisualItem item, MouseEvent e) {
    if (item.canSet(mousePositionKey, Point2D.class))
      setPosition(item, outsidePoint);
  }

  private void handleItem (VisualItem item, MouseEvent e) {
    if (item.canSet(mousePositionKey, Point2D.class)) {
      Display display = (Display) e.getComponent();
      Point2D mouseCoordinates = display.getAbsoluteCoordinate(e.getPoint(), null);
      Rectangle2D itemBounds = item.getBounds();
      Point2D mouseCoordinatesRelative =
        new Point2D.Double(mouseCoordinates.getX() - itemBounds.getX(), mouseCoordinates.getY() - itemBounds.getY());
      setPosition(item, mouseCoordinatesRelative);
    }
  }

  private void setPosition (VisualItem item, Point2D mouseCoordinates) {
    item.set(mousePositionKey, mouseCoordinates);
    if (activity != null)
      item.getVisualization().run(activity);
  }

}
