package p9.binary.cfg.display.items;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

import p9.binary.analysis.slicing.Slice;
import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.StrokeConfig;
import p9.binary.cfg.display.items.instructions.HiddenLinesItem;
import p9.binary.cfg.display.items.instructions.InstructionItem;
import p9.binary.cfg.graph.BasicBlock;
import rreil.disassembler.Instruction;

/**
 *
 * @author Raphael Dümig
 */
public class BasicBlockItem extends ContainerItem {
  private final BasicBlock block;
  private final List<DrawableItem> allItems;
  private Slice slice;
  private final boolean nativeCode;
  private final boolean numbersInHex;

  public BasicBlockItem (BasicBlock block, boolean nativeCode, boolean numbersInHex) {
    super(new TableLayout());
    this.block = block;
    this.allItems = new LinkedList<>();
    this.numbersInHex = numbersInHex;
    this.nativeCode = nativeCode;

    for (Instruction instr : block) {
      allItems.add(new InstructionItem(instr, nativeCode, numbersInHex));
    }

    updateItems();
  }

  private void updateItems() {
    removeAllItems();

    if (slice == null) {
      // no slicing
      addAllItems(allItems);
      return;
    }

    // show only the slice
    List<DrawableItem> tempList = new LinkedList<>();
    HiddenLinesItem hiddenLines = null;

    for (DrawableItem curItem : allItems) {
      if (curItem instanceof InstructionItem) {
        InstructionItem insnItem = (InstructionItem) curItem;

        if (slice.contains(insnItem.getInstruction().address())) {
          if (hiddenLines != null) {
            tempList.add(hiddenLines);
            hiddenLines = null;
          }
          tempList.add(curItem);
        } else {
          if (hiddenLines == null) {
            hiddenLines = new HiddenLinesItem();
          }
          hiddenLines.addLine(curItem);
        }
      } else {
        if (hiddenLines != null) {
          tempList.add(hiddenLines);
          hiddenLines = null;
        }
        tempList.add(curItem);
      }
    }
    if (hiddenLines != null) {
      tempList.add(hiddenLines);
    }

    addAllItems(tempList);
  }

  public BasicBlock getBasicBlock() {
    return block;
  }

  @Override
  public void draw(Graphics2D g, Point2D position) {
    // draw the instructions (the children of this item)
    super.draw(g, position);
    // If we are showing RReil-instructions, draw the separators indicating native instructions.
    // We are drawing these after the instructions, so that the separators
    // will not be hidden by the background color of the instructions.
    if (!nativeCode)
      drawSeparators(g, position);
  }

  private void drawSeparators (Graphics2D g, Point2D position) {
    Stroke oldStroke = g.getStroke();

    // if we are showing an RReil-instruction, draw the separators of
    // native instructions
    if(!nativeCode) {
      g.setColor(ColorConfig.$NativeInsnSeparator);
      g.setStroke(StrokeConfig.$NativeInsnSeparator);
      // draw the instruction separators
      for (double sepPos : getInstructionSeparatorPositions()) {
        g.draw(new Line2D.Double(
                position.getX(),
                position.getY() + sepPos,
                position.getX() + getCurrentSize().getWidth(),
                position.getY() + sepPos)
        );
      }
    }
    g.setStroke(oldStroke);
  }

  private Iterable<Double> getInstructionSeparatorPositions() {
    LinkedList<Double> result = new LinkedList<>();

    // draw the instruction separators
    boolean first = true;

    for(DrawableItem item : this) {
      if(!(item instanceof InstructionItem))
        continue;

      InstructionItem iitem = (InstructionItem) item;
      if(iitem.startsBlock()) {
        if(first)
          first = false;
        else
          result.add(getLayoutPosition(item).getY());
      }
    }

    return result;
  }

  public DrawableItem getItemInLine(int index) {
    return ((TableLayout) lm).getItemInLine(index);
  }

  public void insertComment(CommentItem insertItem, DrawableItem locationItem) {
    allItems.add(allItems.indexOf(locationItem), insertItem);
    updateItems();
  }

  public boolean deleteComment(CommentItem commentItem) {
    boolean success = allItems.remove(commentItem);
    if (success) {
      updateItems();
    }
    return success;
  }

  public void setSlice(Slice slice) {
    this.slice = slice;
    updateItems();
  }

  public void disableSlice() {
    slice = null;
    updateItems();
  }

  public List<InstructionItem> getInstructionItems() {
    LinkedList<InstructionItem> iitems = new LinkedList<>();

    for(DrawableItem ditem : this) {
      if(ditem instanceof InstructionItem) {
        iitems.add((InstructionItem) ditem);
      }
    }
    return iitems;
  }

  @Override
  public JPopupMenu contextMenu(MouseEvent me, final Point2D relPos) {
    // create the popup menu object and append the menu items of the children widgets
    JPopupMenu menu = super.contextMenu(me, relPos);
    // create the menu entry for inserting a comment
    JMenuItem commentItem = new JMenuItem("Insert comment above");
    commentItem.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        CommentDialogPanel dialogPanel = new CommentDialogPanel();
        DialogDescriptor dd = new DialogDescriptor(dialogPanel, "New comment");

        // display the dialog
        if (DialogDisplayer.getDefault().notify(dd) == NotifyDescriptor.OK_OPTION) {
          // get the text from the user
          CommentItem comment = new CommentItem(dialogPanel.getInputText());
          // update the text color of the item
          comment.setTextColor(null);
          BasicBlockItem.this.insertComment(comment, getItemAt(relPos));
          getViewer().redraw();
        }
      }
    });
    menu.add(commentItem);

    if (getItemAt(relPos) instanceof CommentItem) {
      JMenuItem deleteCommentItem = new JMenuItem("Delete comment");
      deleteCommentItem.addActionListener(new ActionListener() {
        @Override public void actionPerformed (ActionEvent e) {
          BasicBlockItem.this.deleteComment((CommentItem) getItemAt(relPos));
          getViewer().redraw();
        }
      });
      menu.add(deleteCommentItem);

      JMenuItem editCommentItem = new JMenuItem("Edit comment");
      editCommentItem.addActionListener(new ActionListener() {
        @Override public void actionPerformed (ActionEvent e) {
          CommentItem comment = (CommentItem) getItemAt(relPos);
          CommentDialogPanel dialogPanel = new CommentDialogPanel(comment.getComment());
          DialogDescriptor dd = new DialogDescriptor(dialogPanel, "New comment");
          // display the dialog
          if (DialogDisplayer.getDefault().notify(dd) == NotifyDescriptor.OK_OPTION) {
            // get the text from the user
            comment.setComment(dialogPanel.getInputText());
            getViewer().redraw();
          }
        }
      });
      menu.add(editCommentItem);
    }
    return menu;
  }

  @SuppressWarnings("serial")
  class CommentDialogPanel extends JPanel {
    private final JTextArea textField;

    CommentDialogPanel() {
      this("");
    }

    CommentDialogPanel(String text) {
      setLayout(new FlowLayout());
      JLabel label = new JLabel("Comment:");
      label.setVerticalTextPosition(JLabel.TOP);
      textField = new JTextArea(text);
      JScrollPane scrollPane = new JScrollPane(textField);
      // setPreferredSize(new Dimension(300,100));
      scrollPane.setPreferredSize(new Dimension(300,100));
      JPanel labelPanel = new JPanel();
      labelPanel.setLayout(new BorderLayout());
      labelPanel.add(label, BorderLayout.NORTH);
      labelPanel.setPreferredSize(new Dimension(100, 100));
      add(labelPanel);
      add(scrollPane);
      scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

      textField.setAutoscrolls(true);
      textField.setFont(BasicBlockItem.this.getFont());
    }

    public String getInputText() {
      return textField.getText();
    }
  }

}
