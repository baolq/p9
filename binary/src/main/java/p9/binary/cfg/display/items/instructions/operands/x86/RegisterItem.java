package p9.binary.cfg.display.items.instructions.operands.x86;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import javalx.data.Option;

import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import p9.binary.analysis.slicing.Slice;
import p9.binary.analysis.slicing.Slicer;
import p9.binary.cfg.InstructionsHelper;
import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.items.BasicBlockItem;
import p9.binary.cfg.display.items.TextItem;
import p9.binary.cfg.display.items.highlight.ObjectHighlight;
import p9.binary.cfg.display.items.instructions.InstructionItem;
import p9.binary.cfg.graph.AnalysisResult;
import p9.binary.cfg.graph.BasicBlock;
import p9.binary.cfg.graph.Cfg;
import p9.binary.cfg.graph.NativeCfg;
import rreil.disassembler.Instruction;
import rreil.lang.Rhs.Rvar;
import bindead.analyses.Analysis;
import bindead.domainnetwork.interfaces.RootDomain;

/**
 * This item shows a register in the native view of the CFG.
 * It should be embedded in a NativeOperandItem.
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class RegisterItem extends TextItem {
  private final String register;

  public RegisterItem (String register) {
    super(register.toString().toLowerCase());
    this.register = register;
    this.highlighted = false;
    super.setTextColor(ColorConfig.$Register);
  }

  @Override public void setTextColor (Color c) {
    // text color is set in constructor and should not be changed anymore
  }

  @Override public void mouseEntered (MouseEvent me, Point2D relPos) {
    super.mouseEntered(me, relPos);
    getViewer().highlight(new ObjectHighlight(register));
  }

  @Override public void mouseExited (MouseEvent me, Point2D relPos) {
    super.mouseExited(me, relPos);
    getViewer().disableHighlight(new ObjectHighlight(register));
  }

  private Instruction getInstruction () {
    InstructionItem instructionItem = (InstructionItem) getParent().getParent().getParent();
    Instruction insn = instructionItem.getInstruction();
    return insn;
  }

  private BasicBlock getBasicBlock () {
    InstructionItem instructionItem = (InstructionItem) getParent().getParent().getParent();
    BasicBlock basicBlock = ((BasicBlockItem) instructionItem.getParent()).getBasicBlock();
    return basicBlock;
  }

  @Override @SuppressWarnings("rawtypes") public String tooltipText () {
    Instruction insn = getInstruction();
    BasicBlock basicBlock = getBasicBlock();
    Cfg cfg = getViewer().getCfg();
    Analysis<?> analysis = cfg.getAnalysis().getAnalysis();
    Option<Rvar> variable = InstructionsHelper.getNativeRegisterRReilVariable(register, analysis.getPlatform());
    if (variable.isNone())
      return "";

    // FIXME: actually we would need to know if the operand is a lhs or rhs
    // only then we can choose if it should be stateAfter or stateBefore
    Option<RootDomain> stateBefore = InstructionsHelper.getStateAfterInsn(insn, cfg, basicBlock);
    // TODO: we might want to show more things for the variable, like thresholds, congruences, etc.
    return InstructionsHelper.variableToHTML(stateBefore, variable.get());
  }

  @Override public JPopupMenu contextMenu (MouseEvent me, final Point2D relPos) {
    final Cfg cfg = getViewer().getCfg();
    final AnalysisResult analysis = cfg.getAnalysis();
    final Option<Rvar> variable =
      InstructionsHelper.getNativeRegisterRReilVariable(register, analysis.getAnalysis().getPlatform());

    JPopupMenu menu = super.contextMenu(me, relPos);
    if (variable.isNone())
      return menu;

    // create the popup menu object and append the menu items of the children widgets
    if (!root.slicingEnabled()) {
      JMenuItem slicingItem = new JMenuItem("Slice for " + variable.get());
      slicingItem.addActionListener(new ActionListener() {
        @Override public void actionPerformed (ActionEvent e) {
          Instruction insn = getInstruction();
          NativeCfg nativeCfg = (NativeCfg) cfg;
          Slice result = Slicer.slice(variable.get(), insn, nativeCfg, analysis);
          if (result != null) {
            getViewer().applySlice(result);
            String message =
              "Showing sliced CFG for " + variable.get() + " at " + insn.address().toStringWithHexPrefix();
            JCheckBox sliceMessageBox = new JCheckBox(message);
            sliceMessageBox.setSelected(true);
            sliceMessageBox.setBackground(ColorConfig.$SliceMessageBg);
            sliceMessageBox.addItemListener(new ItemListener() {
              @Override public void itemStateChanged (ItemEvent ie) {
                if (ie.getStateChange() == ItemEvent.DESELECTED) {
                  getViewer().disableSlice();
                  getViewer().removeFromToolbar("SliceMessage");
                }
              }
            });
            getViewer().addToToolbar("SliceMessage", sliceMessageBox);
          }
        }
      });
      menu.add(slicingItem);
    }
    return menu;
  }

  @Override public void highlight (ObjectHighlight highlight) {
    if (highlight.contains(register)) {
      highlighted = true;
    }
  }

  @Override public void disableHighlight (ObjectHighlight highlighting) {
    if (highlighting.contains(register)) {
      highlighted = false;
    }
  }

}
