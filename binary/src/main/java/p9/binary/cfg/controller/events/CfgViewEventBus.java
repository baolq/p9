package p9.binary.cfg.controller.events;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import p9.binary.cfg.display.CfgComponent;
import p9.binary.cfg.graph.AnalysisResult;
import rreil.lang.RReilAddr;
import bindead.analyses.algorithms.data.CallString;
import bindead.analyses.algorithms.data.ProgramCtx;
import binparse.Binary;

public class CfgViewEventBus {
  private static final CfgViewEventBus instance = new CfgViewEventBus();
  private final List<CfgViewEventListener> listeners = new ArrayList<CfgViewEventListener>();

  public static CfgViewEventBus getInstance () {
    return instance;
  }

  private CfgViewEventBus () {
  }

  public void publish (CfgFocusAddressEvent event) {
    for (CfgViewEventListener listener : listeners) {
      listener.notify(event);
    }
    // if nobody acted on the event then build a view to act on it and re-send the event
    if (!event.isConsumed() && event.getReceiverBuilder() != null) {
      event.getReceiverBuilder().spawnReceiver();
      for (CfgViewEventListener listener : listeners) {
        listener.notify(event);
      }
    }
  }

  public void removeListener (CfgViewEventListener listener) {
    listeners.remove(listener);
  }

  public void addListener (CfgViewEventListener listener) {
    listeners.add(0, listener);
  }

  public static String getNativeEventsID (AnalysisResult analysis, CallString callstring) {
    Binary binary = analysis.getAnalysis().getBinaryCode().getBinary();
    String architecture = binary.getArchitectureName();
    String fileName = binary.getFileName();
    return fileName + " " + architecture + callstring;
  }

  public static String getRReilEventsID (AnalysisResult analysis, CallString callstring) {
    String architecture = "RREIL";
    String fileName = analysis.getAnalyzedFileName();
    return fileName + " " + architecture + callstring;
  }

  public static interface CfgViewEventListener extends EventListener {
    public void notify (CfgFocusAddressEvent event);
  }

  public static interface ReceiverBuilder {
    public void spawnReceiver ();
  }

  public static class CfgFocusAddressEvent extends ViewEvent {
    private final RReilAddr address;
    private final ReceiverBuilder receiverBuilder;

    public CfgFocusAddressEvent (String receiverID, RReilAddr address, ReceiverBuilder builder) {
      super(receiverID);
      this.address = address;
      this.receiverBuilder = builder;
    }

    public RReilAddr getAddress () {
      return address;
    }

    public ReceiverBuilder getReceiverBuilder () {
      return receiverBuilder;
    }

  }

  public static class CfgFocusAddressInRReilCfgEvent extends CfgFocusAddressEvent {
    public CfgFocusAddressInRReilCfgEvent (final AnalysisResult analysis, final ProgramCtx point) {
      super(getRReilEventsID(analysis, point.getCallString()), point.getAddress(), new CfgViewEventBus.ReceiverBuilder() {
        @Override public void spawnReceiver () {
          CfgComponent.showRReilCfg(analysis, point.getCallString());
        }
      });
    }
  }

  public static class CfgFocusAddressInNativeCfgEvent extends CfgFocusAddressEvent {
    public CfgFocusAddressInNativeCfgEvent (final AnalysisResult analysis, final ProgramCtx point) {
      super(getNativeEventsID(analysis, point.getCallString()), point.getAddress(), new CfgViewEventBus.ReceiverBuilder() {
        @Override public void spawnReceiver () {
          CfgComponent.showNativeCfg(analysis, point.getCallString());
        }
      });
    }
  }

}
