package prefuse.render;

import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.RectangularShape;

import prefuse.Constants;
import prefuse.util.ColorLib;
import prefuse.util.GraphicsLib;
import prefuse.visual.VisualItem;

/**
 * @author Bogdan Mihaila
 */
public class HighlightableLineLabelRenderer extends LabelRenderer {
  public static final String $HighlightedLineKey = "highlightedLine";

  public HighlightableLineLabelRenderer () {
    super();
  }

  public HighlightableLineLabelRenderer (String labelKey) {
    super(labelKey);
  }

  @Override public void render (Graphics2D g, VisualItem item) {
    RectangularShape shape = (RectangularShape) getShape(item);
    if (shape == null)
      return;

    // fill the shape, if requested
    int type = getRenderType(item);
    if (type == RENDER_TYPE_FILL || type == RENDER_TYPE_DRAW_AND_FILL)
      GraphicsLib.paint(g, item, shape, getStroke(item), RENDER_TYPE_FILL);

    // now render the image and text
    String text = m_text;
    Image img = getImage(item);

    if (text == null && img == null)
      return;

    double size = item.getSize();
    boolean useInt = 1.5 > Math.max(g.getTransform().getScaleX(),
        g.getTransform().getScaleY());
    double x = shape.getMinX() + size * m_horizBorder;
    double y = shape.getMinY() + size * m_vertBorder;

    // render image
    if (img != null) {
      double w = size * img.getWidth(null);
      double h = size * img.getHeight(null);
      double ix = x, iy = y;

      // determine one co-ordinate based on the image position
      switch (m_imagePos) {
      case Constants.LEFT:
        x += w + size * m_imageMargin;
        break;
      case Constants.RIGHT:
        ix = shape.getMaxX() - size * m_horizBorder - w;
        break;
      case Constants.TOP:
        y += h + size * m_imageMargin;
        break;
      case Constants.BOTTOM:
        iy = shape.getMaxY() - size * m_vertBorder - h;
        break;
      default:
        throw new IllegalStateException(
          "Unrecognized image alignment setting.");
      }

      // determine the other coordinate based on image alignment
      switch (m_imagePos) {
      case Constants.LEFT:
      case Constants.RIGHT:
        // need to set image y-coordinate
        switch (m_vImageAlign) {
        case Constants.TOP:
          break;
        case Constants.BOTTOM:
          iy = shape.getMaxY() - size * m_vertBorder - h;
          break;
        case Constants.CENTER:
          iy = shape.getCenterY() - h / 2;
          break;
        }
        break;
      case Constants.TOP:
      case Constants.BOTTOM:
        // need to set image x-coordinate
        switch (m_hImageAlign) {
        case Constants.LEFT:
          break;
        case Constants.RIGHT:
          ix = shape.getMaxX() - size * m_horizBorder - w;
          break;
        case Constants.CENTER:
          ix = shape.getCenterX() - w / 2;
          break;
        }
        break;
      }

      if (useInt && size == 1.0) {
        // if possible, use integer precision
        // results in faster, flicker-free image rendering
        g.drawImage(img, (int) ix, (int) iy, null);
      } else {
        m_transform.setTransform(size, 0, 0, size, ix, iy);
        g.drawImage(img, m_transform, null);
      }
    }

    // render text
    int textColor = item.getTextColor();
    if (text != null && ColorLib.alpha(textColor) > 0) {
      g.setPaint(ColorLib.getColor(textColor));
      g.setFont(m_font);
      FontMetrics fm = DEFAULT_GRAPHICS.getFontMetrics(m_font);

      // compute available width
      double tw;
      switch (m_imagePos) {
      case Constants.TOP:
      case Constants.BOTTOM:
        tw = shape.getWidth() - 2 * size * m_horizBorder;
        break;
      default:
        tw = m_textDim.width;
      }

      // compute available height
      double th;
      switch (m_imagePos) {
      case Constants.LEFT:
      case Constants.RIGHT:
        th = shape.getHeight() - 2 * size * m_vertBorder;
        break;
      default:
        th = m_textDim.height;
      }

      // compute starting y-coordinate
      y += fm.getAscent();
      switch (m_vTextAlign) {
      case Constants.TOP:
        break;
      case Constants.BOTTOM:
        y += th - m_textDim.height;
        break;
      case Constants.CENTER:
        y += (th - m_textDim.height) / 2;
      }

      int highlightedLine = -1;
      if (item.canGetInt($HighlightedLineKey))
        highlightedLine = item.getInt($HighlightedLineKey);
      int line = 0;
      boolean highlight = false;

      // render each line of text
      int lh = fm.getHeight(); // the line height
      int start = 0, end = text.indexOf(m_delim);
      for (; end >= 0; y += lh) {
        highlight = (line == highlightedLine);
        drawString(g, fm, text.substring(start, end), useInt, x, y, tw, highlight);
        start = end + 1;
        end = text.indexOf(m_delim, start);
        line = line + 1;
      }
      highlight = (line == highlightedLine);
      drawString(g, fm, text.substring(start), useInt, x, y, tw, highlight);
    }

    // draw border
    if (type == RENDER_TYPE_DRAW || type == RENDER_TYPE_DRAW_AND_FILL) {
      GraphicsLib.paint(g, item, shape, getStroke(item), RENDER_TYPE_DRAW);
    }
  }

  // TODO: find some interesting highlighting styles and make them selectable
  private final void drawString (Graphics2D g, FontMetrics fm, String text,
      boolean useInt, double x, double y, double w, boolean highlight)
  {
    // compute the x-coordinate
    double tx;
    int stringWidth = fm.stringWidth(text); // TODO: or use itemwidth? because not all the strings are equally long
    switch (m_hTextAlign) {
    case Constants.LEFT:
      tx = x;
      break;
    case Constants.RIGHT:
      tx = x + w - stringWidth;
      break;
    case Constants.CENTER:
      tx = x + (w - stringWidth) / 2;
      break;
    default:
      throw new IllegalStateException(
        "Unrecognized text alignment setting.");
    }
    // use integer precision unless zoomed-in
    // results in more stable drawing
    if (useInt) {
      g.drawString(text, (int) tx, (int) y);
      if (highlight)
        g.drawLine((int) tx, (int) y, (int) tx + stringWidth, (int) y);
    } else {
      g.drawString(text, (float) tx, (float) y);
      if (highlight)
        g.drawLine((int) tx, (int) y, (int) tx + stringWidth, (int) y);
    }
  }

}
