
package p9.binary.cfg.display.items.instructions.operands.x86;

import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.display.items.instructions.operands.OperandsItem;
import rreil.disassembler.Instruction;
import rreil.disassembler.OperandTree;
import rreil.disassembler.OperandTree.Node;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class NativeOperandsItem extends OperandsItem {

  private final boolean numbersInHex;
  private final boolean leaInstruction;
  private final Instruction instr;

  public NativeOperandsItem(Instruction instr, boolean numbersInHex, boolean leaInstruction) {
    super();

    this.numbersInHex = numbersInHex;
    this.leaInstruction = leaInstruction;
    this.instr = instr;

    updateOperands();
  }

  @Override
  public String getOperandsString() {
    StringBuilder builder = new StringBuilder();
    String strSeparator = ", ";
    boolean first = true;

    for (DrawableItem item : this) {
      if (item == null) {
        continue;
      }
      
      if (first) {
        first = false;
      } else {
        builder.append(strSeparator);
      }

      builder.append(item.toString());
    }

    return builder.toString();
  }

  @Override
  public String toString() {
    return getOperandsString();
  }

  private void updateOperands() {
    removeAllItems();

    for(OperandTree op: instr.operands()) {
      Node operandroot = op.getRoot();

      NativeOperandItem oItem = null;

      // check if the operand is a pointer
      if (operandroot.getType() == OperandTree.Type.Size &&
              operandroot.getChildren().get(0).getType() == OperandTree.Type.Mem) {
        oItem = new PointerItem(op, numbersInHex, leaInstruction);
      } else {
        oItem = new NativeOperandItem(op, numbersInHex);
      }

      oItem.updateOperands();
      addItem(oItem);
    }
  }
}
