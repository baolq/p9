package p9.binary.cfg.display.items;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import p9.binary.cfg.display.ColorConfig;

/**
 * @author Raphael Dümig
 */
public class CommentItem extends TextItem {
  private String comment;
  private boolean hovered;

  public CommentItem (String commentText) {
    this(commentText, false);
  }

  public CommentItem (String commentText, boolean inline) {
    super(formatComment(commentText));
    this.comment = commentText;
    this.hovered = false;

    setPadding(PADDING_HORIZONTAL, 7);
    if (!inline) {
      setPadding(PADDING_VERTICAL, 2);
    }
  }

  public String getComment () {
    return comment;
  }

  public void setComment (String newComment) {
    this.comment = newComment;
    super.setText(formatComment(comment));
  }

  @Override public void setTextColor (Color c) {
    if(hovered) {
      super.setTextColor(ColorConfig.$CommentHighlight);
    }
    else {
      super.setTextColor(ColorConfig.$Comment);
    }
  }

  @Override public void mouseEntered (MouseEvent me, Point2D relPos) {
    super.mouseEntered(me, relPos);
    hovered = true;
    setBackgroundColor(ColorConfig.$MouseHoverHighlight);
    setTextColor(null);
  }

  @Override public void mouseExited (MouseEvent me, Point2D relPos) {
    super.mouseExited(me, relPos);
    hovered = false;
    disableBackgroundColor();
    setTextColor(null);
  }
  
  private static String formatComment(String comment) {
    if(!comment.contains("\n")) {
      return "// " + comment;
    }
    else {
      return "/* " + comment.replaceAll("\n","\n * ") + "\n */";
    }
  }
}
