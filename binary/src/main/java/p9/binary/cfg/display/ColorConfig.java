package p9.binary.cfg.display;

import java.awt.Color;

import prefuse.util.ColorLib;

public class ColorConfig {
  public static final Color $Background = Color.white;
  public static final Color $Foreground = Color.black;
  public static final int $NormalFg = ColorLib.hex("#DCDCCC");
  public static final int $NormalBg = ColorLib.hex("#3F3F3F");
  public static final int $NodeFg = ColorLib.color(Color.black);
  public static final int $NodeBg = ColorLib.hex("#EBFFC9");
  public static final int $CursorFg = ColorLib.hex("#000d18");
  public static final int $CursorBg = ColorLib.hex("#8faf9f");
  public static final int $SearchFg = ColorLib.hex("#284f28");
  public static final int $SearchBg = ColorLib.hex("#A4B6C8");
  public static final int $FocusFg = ColorLib.color(Color.black);
  public static final int $FocusBg = ColorLib.hex("#FEF2DA");
  public static final int $DOIFg = ColorLib.hex("#709080");
  public static final int $DOIBg = ColorLib.hex("#ADBB93");
  public static final int $Edge = ColorLib.color(Color.black);
  public static final int $FunctionHighlight = ColorLib.hex("#B25248");
  public static final int $EdgeHighlight = ColorLib.hex("#B90000");
  public static final int $NodeBorder = ColorLib.hex("#585858");
  public static final Color $RectangleHighlight = ColorLib.getColor(ColorLib.hex("#ADE4B0"));
  public static final Color $MouseHoverHighlight = ColorLib.getColor(ColorLib.hex("#BDD4E4"));
  public static final Color $TextHighlight = ColorLib.getColor(ColorLib.hex("#C20000"));
  public static final Color $TextHighlightBackground = ColorLib.getColor(ColorLib.hex("#45F77D"));
  public static final Color $NodeShadow = ColorLib.getColor(ColorLib.hex("#9C9E9B"));
  public static final Color $Address = ColorLib.getColor(ColorLib.hex("#646464"));
  public static final Color $Comment = ColorLib.getColor(ColorLib.hex("#A4A4A4"));
  public static final Color $CommentHighlight = Color.white;
  public static final Color $Opcode = ColorLib.getColor(ColorLib.hex("#646464"));
  public static final Color $RReilSizeAnnotation = ColorLib.getColor(ColorLib.hex("#646464"));
  public static final Color $Mnemonic = ColorLib.getColor(ColorLib.hex("#0000AD"));
  public static final Color $Immediate = Color.blue;
  public static final Color $Register = ColorLib.getColor(ColorLib.hex("#960000"));
  public static final Color $ColumnSeparator = ColorLib.getColor(ColorLib.hex("#C2C2C2"));
  public static final Color $NativeInsnSeparator = ColorLib.getColor(ColorLib.hex("#C2C2C2"));
  public static final Color $InsnWarning = ColorLib.getColor(ColorLib.hex("#FFC0CB"));
  public static final Color $SliceMessageBg = ColorLib.getColor(ColorLib.hex("#C05800"));
  public static final Color $IterationsDiagramAddressMarker = Color.blue;
  public static final Color $IterationsDiagramIterationsMarker = Color.red;
}
