package p9.binary;

import java.io.IOException;

import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

@NbBundle.Messages({
    "LBL_RREIL_LOADER=RREIL Code"
})
@MIMEResolver.ExtensionRegistration(
        displayName = "#LBL_RREIL_LOADER",
        mimeType = RReilFileDataObject.mimeType,
        extension = {"rreil", "RREIL"},
        showInFileChooser = "RREIL Code"
)
@DataObject.Registration(
        displayName = "#LBL_RREIL_LOADER",
        mimeType = RReilFileDataObject.mimeType,
        iconBase = "p9/binary/icons/train_icon_black_white_line-16px.png",
        position = 300
)
@ActionReferences({
    @ActionReference(
            path = "Loaders/text/x-rreil/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
            position = 100,
            separatorAfter = 200
    ),
    @ActionReference(
            path = "Loaders/text/x-rreil/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
            position = 300
    ),
    @ActionReference(
            path = "Loaders/text/x-rreil/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
            position = 400,
            separatorAfter = 500
    ),
    @ActionReference(
            path = "Loaders/text/x-rreil/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
            position = 600
    ),
    @ActionReference(
            path = "Loaders/text/x-rreil/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
            position = 700,
            separatorAfter = 800
    ),
    @ActionReference(
            path = "Loaders/text/x-rreil/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
            position = 900,
            separatorAfter = 1000
    ),
    @ActionReference(
            path = "Loaders/text/x-rreil/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
            position = 1100,
            separatorAfter = 1200
    ),
    @ActionReference(
            path = "Loaders/text/x-rreil/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
            position = 1300
    ),
    @ActionReference(
            path = "Loaders/text/x-rreil/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
            position = 1400
    )
})
public class RReilFileDataObject extends MultiDataObject {

    public static final String mimeType = "text/x-rreil";

    public RReilFileDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        registerEditor(mimeType, true);
    }

    @Override
    protected Node createNodeDelegate() {
        return new RReilObjectNode(this);
    }

    @Override
    protected int associateLookup() {
        return 1;
    }

    @MultiViewElement.Registration(
            displayName = "#LBL_RREIL_EDITOR",
            iconBase = "p9/binary/icons/train_icon_black_white_line-16px.png",
            mimeType = mimeType,
            persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
            preferredID = "RREIL-Code",
            position = 1000
    )
    @NbBundle.Messages("LBL_RREIL_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }
}
