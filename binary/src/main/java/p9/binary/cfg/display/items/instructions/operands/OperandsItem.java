package p9.binary.cfg.display.items.instructions.operands;

import p9.binary.cfg.display.items.ContainerItem;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public abstract class OperandsItem extends ContainerItem {
  
  public OperandsItem() {
    super(new OperandsLayout());
  }
  
  public abstract String getOperandsString();
}
